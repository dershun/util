<?php
/*
 * @Author: juneChen && junechen_0606@163.com
 * @Date: 2023-06-28 12:03:17
 * @LastEditors: juneChen && junechen_0606@163.com
 * @LastEditTime: 2023-06-29 17:07:06
 * @Description: 雪花算法类
 * 
 */

declare(strict_types=1);

namespace Dershun\Util;

class SnowFlake
{
    //开始时间
    private static $twepoch = 1687937677;
    // 数据中心(机房) id
    private static $datacenterId = 0;
    // 机器ID
    private static $workerId = 0;
    // 同一时间的序列
    private static $sequence = 0;
    // 最近一次时间戳
    private static $lastTimestamp = 0;
    // 机房号，的ID所占的位数 5个bit 最大:11111(2进制)--> 31(10进制)
    // 5 bit最多只能有31个数字，机房id最多只能是32以内
    private static $maxDatacenterId = 32;
    // 机器ID所占的位数 5个bit 最大:11111(2进制)--> 31(10进制)
    // 5 bit最多只能有31个数字，就是说机器id最多只能是32以内
    private static $maxWorkerId = 32;
    // 同一时间的序列所占的位数 12个bit 111111111111(2进制) = 4095(10进制) 最多就是同一毫秒生成4096个
    // 用于序号的与运算，保证序号最大值在0-4095之间
    private static $maxSequence = 4095;

    /**
     * Undocumented function
     *
     * @param integer $datacenterId
     * @param integer $workerId
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    public static function setConfig(int $datacenter, int $worker): void
    {
        if ($datacenter > self::$maxDatacenterId || $datacenter < 0) {
            throw new \DomainException(
                sprintf(
                    "datacenter Id can't be greater than %d or less than 0",
                    self::$maxDatacenterId
                )
            );
        }

        if ($worker > self::$maxWorkerId || $worker < 0) {
            throw new \DomainException(
                sprintf(
                    "worker Id can't be greater than %d or less than 0",
                    self::$maxWorkerId
                )
            );
        }

        self::$datacenterId = $datacenter;
        self::$workerId = $worker;
    }

    /**
     * 生成唯一ID
     *
     * @return Integer
     * @author juneChen <junechen_0606@163.com>
     */
    public static function createId(): int
    {
        // 获取当前时间戳，单位毫秒
        $timestamp = self::currentTime();

        if ($timestamp < self::$lastTimestamp) {
            throw new \DomainException(
                sprintf(
                    'Clock moved backwards.  Refusing to generate id for %d milliseconds',
                    self::$lastTimestamp - $timestamp
                )
            );
        }

        // 去重
        if (self::$lastTimestamp == $timestamp) {
            self::$sequence += 1;
            // sequence序列大于4095
            if (self::$sequence == self::$maxSequence) {
                // 调用到下一个时间戳的方法
                $timestamp = self::tilNextMillisecond(self::$lastTimestamp);
            }
        } else {
            // 如果是当前时间的第一次获取，那么就置为0
            self::$sequence = 0;
        }

        // 记录上一次的时间戳
        self::$lastTimestamp = $timestamp;

        // 偏移计算
        return (($timestamp - self::$twepoch) << 22) |
            (self::$datacenterId << 17) |
            (self::$workerId << 12) |
            self::$sequence;
    }

    // 获取机器ID
    public static function getWorkerId(): int
    {
        return self::$workerId;
    }

    // 获取机房ID
    public static function getDatacenterId(): int
    {
        return self::$datacenterId;
    }


    // 获取最新一次获取的时间戳
    public static function getLastTimestamp(): int
    {
        return self::$lastTimestamp;
    }

    /**
     * 获取下一毫秒值
     *
     * @param integer $lastTimestamp
     * @return integer
     * @author juneChen <junechen_0606@163.com>
     */
    private static function tilNextMillisecond(int $lastTimestamp): int
    {
        // 获取最新时间戳
        $timestamp = self::currentTime();
        // 如果发现最新的时间戳小于或者等于序列号已经超4095的那个时间戳
        while ($timestamp <= $lastTimestamp) {
            // 不符合则继续
            $timestamp = self::currentTime();
        }
        return $timestamp;
    }

    /**
     * 获取毫秒值
     *
     * @return integer
     * @author juneChen <junechen_0606@163.com>
     */
    private static function currentTime(): int
    {
        return intval(microtime(true) * 1000);
    }
}
