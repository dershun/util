<?php

namespace Dershun\Util;

/**
 * Layui 框架使用类
 * @author Dershun <dershun@163.com>
 */
class Layui
{

    /**
     * 递归解析 tree 选中信息
     * @param array $trees 节点数据
     * @param int $pid 上级节点id
     * @return array 解析成可以写入数据库的格式
     */
    public static function parseTrees($trees = [], $pid = 0)
    {
        $sort   = 1;
        $result = [];
        foreach ($trees as $tree) {
            $result[] = $tree;
            if (isset($tree['children'])) {
                $result = array_merge($result, self::parseTrees($tree['children'], $tree['id']));
            }
            $sort ++;
        }
        return $result;
    }
}