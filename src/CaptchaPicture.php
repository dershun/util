<?php
/*
 * @Author: juneChen && junechen_0606@163.com
 * @Date: 2023-06-28 12:03:17
 * @LastEditors: juneChen && junechen_0606@163.com
 * @LastEditTime: 2023-07-03 19:24:57
 * @Description: 图片验证码类
 * 
 */

declare(strict_types=1);

namespace Dershun\Util;

class CaptchaPicture
{
    /**
     * 配置
     *
     * @var array
     */
    private $config = [
        // 图片宽度
        'width'  => 150,
        // 图片高度
        'height' => 60,
        // 验证码过期时间（s）
        'expire' => 600,
        //使用算术验证码
        'math'   => false,
        // 验证码字体大小(px)
        'fontSize'  => 5,
        // 是否画混淆曲线
        'curve' => true,
        // 是否添加杂点
        'noise' => true,
        // 验证码位数
        'length' => 5,
        // 背景颜色
        'bg' => [255, 255, 255],
        // 字体验证 [243, 251, 254] | []
        'color' => []
    ];

    //生成验证码
    private $Code = "";
    /**
     * 验证码对应的key
     *
     * @var string
     */
    private $codeKey = "";

    /**
     * 图片处理类
     *
     * @var \GdImage
     */
    private $img;

    private $offsetX = 15;
    private $offsetY = 20;

    /**
     * 构造方法
     *
     * @param array $config 配置数组
     * @author juneChen <junechen_0606@163.com>
     */
    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * 获取流数据
     *
     * @return string
     * @author juneChen <junechen_0606@163.com>
     */
    public function base64(): string
    {
        ob_start();
        imagepng($this->img);
        imagedestroy($this->img);
        $ImageData = ob_get_clean();
        return 'data:image/jpg;base64,' . base64_encode($ImageData);
    }

    /**
     * 直接输出到浏览器
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    public function imagepng(): void
    {
        imagepng($this->img);
        imagedestroy($this->img);
    }

    /**
     * 创建验证码
     *
     * @return this
     * @author juneChen <junechen_0606@163.com>
     */
    public function captcha()
    {
        $this->img = imagecreatetruecolor($this->config['width'], $this->config['height']);
        $this->setBackground();
        if ($this->config['noise']) {
            $this->noise();
        }
        if ($this->config['curve']) {
            $this->curve();
        }
        if ($this->config['math']) {
            $this->calculate();
        } else {
            $this->string();
        }

        $this->codeKey = mt_rand(100, 999) . intval(microtime(true) * 1000) . mt_rand(100, 999);

        return $this;
    }

    /**
     * 修改配置
     *
     * @param array $config 配置数组
     * @return this
     * @author juneChen <junechen_0606@163.com>
     */
    public function setConfig(array $config)
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }

    /**
     * 获取验证码
     *
     * @return string|int
     * @author juneChen <junechen_0606@163.com>
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * 获取验证码KEY
     *
     * @return string|int
     * @author juneChen <junechen_0606@163.com>
     */
    public function getCodeKey()
    {
        return $this->codeKey;
    }

    /**
     * 设置图片背景颜色
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    private function setBackground(): void
    {
        list($R, $G, $B) = $this->config['bg'];
        $black = imagecolorallocate($this->img, $R, $G, $B);
        imagefilledrectangle($this->img, 0, 0, $this->config['width'], $this->config['height'], $black);
    }

    /**
     * 生成干扰点
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    private function noise(): void
    {
        for ($i = 0; $i < 160; $i++) {
            $gray = imagecolorallocate($this->img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imagesetpixel($this->img, mt_rand() % $this->config['width'], mt_rand() % $this->config['height'], $gray);
        }
    }

    /**
     * 生成干扰线条
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    private function curve(): void
    {
        for ($yy = 0; $yy < 10; $yy++) {
            $gray = imagecolorallocate($this->img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
            imageline($this->img, mt_rand(0, $this->config['width']), mt_rand(0, $this->config['height']), mt_rand(0, $this->config['width']), mt_rand(0, $this->config['height']), $gray);
        }
    }

    /**
     * 运算验证码
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    private function calculate(): void
    {
        /*随机生成两个数字*/
        $num1 = mt_rand(10, 20);
        $num2 = mt_rand(1, 9);
        $operator = ["+", "-"][mt_rand(0, 1)];
        eval("\$this->Code = $num1 $operator $num2;");
        $red = $this->color();
        $white = imagecolorallocate($this->img, 247, 46, 46);
        $width = intval($this->config['width'] / 5);
        $height = $this->config['height'] - $this->offsetY;
        /*将计算验证码写入到图片中*/
        imagestring($this->img, $this->config['fontSize'], $width  - $this->offsetX, mt_rand(0, $height), (string) $num1, $red);
        imagestring($this->img, $this->config['fontSize'], $width * 2  - $this->offsetX, mt_rand(0, $height), $operator, $red);
        imagestring($this->img, $this->config['fontSize'], $width * 3  - $this->offsetX, mt_rand(0, $height), (string) $num2, $red);
        imagestring($this->img, $this->config['fontSize'], $width * 4  - $this->offsetX, mt_rand(0, $height), "=", $red);
        imagestring($this->img, $this->config['fontSize'], $width * 5  - $this->offsetX, mt_rand(0, $height), "?", $white);
    }

    /**
     * 字符验证码
     *
     * @return void
     * @author juneChen <junechen_0606@163.com>
     */
    private function string(): void
    {
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234565789";

        $width = intval($this->config['width'] / $this->config['length']);
        $height = $this->config['height'] - $this->offsetY;
        for ($i = 0; $i < $this->config['length']; $i++) {
            $red = $this->color();
            $char = $str[mt_rand(0, 62)];
            imagestring($this->img, $this->config['fontSize'], $width * ($i + 1)  - $this->offsetX, mt_rand(0, $height), $char, $red);
            $this->Code .= $char;
        }
    }

    function color()
    {
        if (empty($this->config['color'])) {
            list($R, $G, $B) = [mt_rand(0, 150), mt_rand(0, 150), mt_rand(0, 150)];
        } else {
            list($R, $G, $B) = $this->config['color'];
        }
        return imagecolorallocate($this->img, $R, $G, $B);
    }
}
