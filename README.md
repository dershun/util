# util

#### 介绍
一些 php 常用工具类

#### 软件架构
软件架构说明


#### 安装

```
composer require dershun/util
```
#### 类说明

1. Tree 树结构生成类  

``` 
use Dershun\Util\Tree 
```

2. NestableJs  jQuery的Nestable 使用类

``` 
use Dershun\Util\NestableJs 
```
3. Layui  Layui 框架使用类

``` 
use Dershun\Util\Layui 
```
4. SnowFlake 雪花算法类

``` 
use Dershun\Util\SnowFlake 

SnowFlake::setConfig(1, 1);
$orderNum = SnowFlake::createId(); // 生成唯一ID
dump($orderNum);
```
5. CaptchaPicture 图片验证码类

``` 
use Dershun\Util\CaptchaPicture 

$Captcha = new CaptchaPicture([
    'width'  => 90,
    'height' => 38,
    "math" => true,
    "curve" => false,
]);

// 返回字节流
$result = $Captcha->captcha()->base64();
$code = $Captcha->getCode();
$codeKey = $Captcha->getCodeKey();

// 直接输出到浏览器
$result = $Captcha->captcha();
$code = $Captcha->getCode();
$codeKey = $Captcha->getCodeKey();
$result->imagepng();
```