<?php

namespace Dershun\Util;

/**
 * jQuery之Nestable 使用类
 * @author Dershun <dershun@163.com>
 */
class NestableJs
{

    /**
     * 获取嵌套式节点
     * @param  array  $list 原始节点数组
     * @return string
     */
    public static function getNestingNode(array $list, int $max_level = 0, int $pid = 0, int $curr_level = 1):string
    {   
        $result = '';
        if($curr_level == 1){
            $result .= '<style type="text/css">.dd3-content .action{display:none;margin-left:.25rem!important;}.dd3-content:hover .action{display:inline-block;}.dd3-content .action span{cursor:pointer;margin-right:.4rem!important;}.text-info{color:#22b9ff!important;}.text-linkedin{color:#0077b5!important;}.text-success{color:#10b759!important;}.text-warning{color:#ffb822!important;}.text-danger{color:#ff3f3f!important;}.dd3-handle{cursor:move;}.dd3-content .title{margin-right:.5rem!important;}.dd3-content .title i{margin-right:.25rem!important;}.dd3-content .name{margin-right:.5rem!important;}.dd3-content .name i{margin-right:.25rem!important;}.dd-disable .dd3-content,.dd-disable .dd3-handle{background: #FFD5D0;color: #D2847B;}</style><ol class="dd-list">';
        }

        foreach ($list as $key => $value) {
            if ($value["pid"] == $pid) {
                $disable  = $value['status'] != 0 ? 'dd-disable' : '';
                // 组合节点
                $result .= '<li class="dd-item dd3-item '.$disable.'" data-id="' . $value['id'] . '">';
                        $result .= '<div class="dd-handle dd3-handle"></div>';
                        $result .= '<div class="dd3-content">';
                          $result .= '<span class="title"><i class="' . $value['icon'] . ' fa-fw"></i>' . $value['title'] . '</span>';
                          if(!empty($value['name'])){
                            $name = $value['name'];
                            if(!empty($value['parameter'])){
                                $name .= "?" . $value['parameter'];
                            }
                            $result .= '<span class="name"><i class="fa fa-send-o fa-fw"></i>' . $name . '</span>';
                          }
                          $result .= '<div class="action">';
                            $result .= '<span class="text-info addSonPermission" data-toggle="tooltip" data-original-title="新增子权限" title="新增子权限"><i class="fa fa-plus-circle"></i></span>';
                            $result .= '<span class="text-linkedin editPermission" data-toggle="tooltip" data-original-title="修改权限" title="修改权限"><i class="fa fa-pencil"></i></span>';

                            if($value['status'] == 0){
                                $result .= '<span class="text-warning permissionStatus" data-status="1" data-toggle="tooltip" data-original-title="禁用权限" title="禁用权限"><i class="fa fa-ban"></i></span>';
                            }else{
                                $result .= '<span class="text-success permissionStatus" data-status="0" data-toggle="tooltip" data-original-title="启用权限" title="启用权限"><i class="fa fa-check-circle-o"></i></span>';
                            }

                            $result .= '<span class="text-danger delPermission" title="删除权限" data-toggle="tooltip" data-original-title="删除权限" ><i class="fa fa-trash-o"></i></span>';
                          $result .= '</div>';
                        $result .= '</div>';

                if ($max_level == 0 || $curr_level != $max_level) {
                    unset($list[$key]);
                    // 下级节点
                    $children = self::getNestingNode($list, $max_level, $value["id"], $curr_level+1);
                    if ($children != '') {
                        $result .= '<ol class="dd-list">' . $children . '</ol>';
                    }
                }

                $result .= '</li>';
            }
        }

        if($curr_level == 1){
            $result .= '</ol>';
        }
        return $result;
    }


    /**
     * 递归解析节点
     * @param array $node 节点数据
     * @param int $pid 上级节点id
     * @return array 解析成可以写入数据库的格式
     */
    public static function parseNode($nodes = [], $pid = 0)
    {
        $sort   = 1;
        $result = [];
        foreach ($nodes as $node) {
            $result[] = [
                'id'   => (int)$node['id'],
                'pid'  => (int)$pid,
                'sort' => $sort,
            ];
            if (isset($node['children'])) {
                $result = array_merge($result, self::parseNode($node['children'], $node['id']));
            }
            $sort ++;
        }
        return $result;
    }
}