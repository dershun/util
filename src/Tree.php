<?php

namespace Dershun\Util;

/**
 * 树结构生成类
 * @author Dershun <dershun@163.com>
 */
class Tree
{

    /**
     * 配置参数
     * @var array
     */
    protected static $config = [
        'id'       => 'id',    // 主键名称
        'pid'      => 'pid',   // 父级键名称
        'children' => 'children', // 子元素键名
        'html'     => '┠ ',   // 层级标记
        'step'     => 4,       // 层级步进数量
    ];

    /**
     * 架构函数
     * @param array $config
     */
    public function __construct($config = [])
    {
        self::$config = array_merge(self::$config, $config);
    }

    /**
     * 配置参数
     * @param  array $config
     * @return object
     */
    public static function config($config = [])
    {
        if (!empty($config)) {
            $config = array_merge(self::$config, $config);
        }
        return new static($config);
    }

    /**
     * 引用方式将数组转树形
     * @param  array   $list  数组
     * @param  integer $root  
     * @return array
     */
    public static function quoteToTree(array $list, $root = 0) {
        $tree = [];
        $refer = [];

        foreach ($list as $key => $data) {
            $refer[ $data[ self::$config['id'] ] ] = &$list[ $key ];
        }

        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[ self::$config['pid'] ];

            if ($root == $parentId) {
                $tree[ $data[ self::$config['id'] ] ] = &$list[ $key ];
            } else {
                if (isset($refer[$parentId])) {
                    $parent = &$refer[ $parentId ];
                    $parent[ self::$config['children'] ][ $data[ self::$config['id'] ] ] = &$list[ $key ];
                    $parent[ self::$config['children'] ] = array_values($parent[ self::$config['children'] ]);
                }
            }
        }

        return $tree;
    }

    /**
     * 将数据集格式化成层次结构
     * @param  array|object   $lists 要格式化的数据集，可以是数组，也可以是对象
     * @param int $pid 父级id
     * @param int $max_level 最多返回多少层，0为不限制
     * @param int $curr_level 当前层数
     * @return array
     */
    public static function toTree($lists = [], $pid = 0, $max_level = 0, $curr_level = 0)
    {
        $trees = [];
        $lists = array_values($lists);
        foreach ($lists as $key => $data) {
            if ($data[self::$config['pid']] == $pid) {
                if ($max_level > 0 && $curr_level == $max_level) {
                    return $trees;
                }
                unset($lists[$key]);
                $child = self::toTree($lists, $data[self::$config['id']], $max_level, $curr_level + 1);
                if (!empty($child)) {
                    $data[self::$config['children']] = $child;
                }
                $trees[] = $data;
            }
        }
        return $trees;
    }

    /**
     * 将数据集格式化成列表结构
     * @param  array|object   $lists 要格式化的数据集，可以是数组，也可以是对象
     * @param  integer $pid        父级id
     * @param  integer $level      级别
     * @return array 列表结构(一维数组)
     */
    public static function toTreeList($lists = [], $pid = 0, $level = 0)
    {
        if (is_array($lists)) {
            $trees = [];
            foreach ($lists as $key => $data) {
                if ($data[self::$config['pid']] == $pid) {
                    $title_prefix   = str_repeat("&nbsp;", $level * self::$config['step']).self::$config['html'];
                    $data['level'] = $level + 1;
                    $data['title_prefix']  = $level == 0 ? '' : $title_prefix;
                    $trees[] = $data;
                    unset($lists[$key]);
                    $trees   = array_merge($trees, self::toTreeList($lists, $data[self::$config['id']], $level + 1));
                }
            }
            return $trees;
        } else {
            foreach ($lists as $key => $data) {
                if ($data[self::$config['pid']] == $pid && is_object($data)) {
                    $title_prefix   = str_repeat("&nbsp;", $level * self::$config['step']).self::$config['html'];
                    $data['level'] = $level + 1;
                    $data['title_prefix']  = $level == 0 ? '' : $title_prefix;
                    $lists->offsetUnset($key);
                    $lists[] = $data;
                    self::toTreeList($lists, $data[self::$config['id']], $level + 1);
                }
            }
            return $lists;
        }
    }

    /**
     * 返回所有父节点
     * @param  array  $lists 数据集
     * @param  string $id    子节点id
     * @return array
     */
    public static function getParents($lists = [], $id = '')
    {
        $trees = [];
        foreach ($lists as $data) {
            if ($data[self::$config['id']] == $id) {
                $trees[] = $data;
                $trees   = array_merge(self::getParents($lists, $data[self::$config['pid']]), $trees);
            }
        }
        return $trees;
    }

    /**
     * 获取所有子节点id
     * @param  array  $lists 数据集
     * @param  string $pid   父级id
     * @return array
     */
    public static function getChildrenId($lists = [], $pid = '')
    {
        $result = [];
        foreach ($lists as $data) {
            if ($data[self::$config['pid']] == $pid) {
                $result[] = $data[self::$config['id']];
                $result = array_merge($result, self::getChildrenId($lists, $data[self::$config['id']]));
            }
        }
        return $result;
    }

    /**
     * 获取所有子节点
     * @param  array  $lists 数据集
     * @param  string $pid   父级id
     * @return array
     */
    public static function getChildren($lists = [], $pid = '')
    {
        $result = [];
        foreach ($lists as $data) {
            if ($data[self::$config['pid']] == $pid) {
                $result[] = $data;
                $result = array_merge($result, self::getChildren($lists, $data[self::$config['id']]));
            }
        }
        return $result;
    }
}